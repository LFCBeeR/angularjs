var app = angular.module('myApp', []);
app.controller('MyController', function($scope) {
$scope.visible = true;
$scope.toggle = function() {
    $scope.visible = !$scope.visible;
  }
});
